#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define GUESS_TIME 10    //最多可猜的次数
#define MAX_NUM    100   //数字范围
int main(void)
{
  int times;
  int num, guess_num;
  char order;
  while((order = getchar()) != 'q')
  {
      times = 0;
      printf("GAME_START\n");
      num = rand() % (MAX_NUM + 1);
      guess_num = -1;
      while(guess_num != num && times < GUESS_TIME)
      {
          if(guess_num != -1)
          {
             if(++ times >= GUESS_TIME)
             {
                 printf("您没有机会了！");
                 goto fail;
             }
             if(guess_num > num)
                 printf("您猜大了，请重试！还有%d次机会；\n", GUESS_TIME - times);
             else if(guess_num < num)
                 printf("您猜的小了，请重试！还有%d次机会；\n", GUESS_TIME - times);
          }
          printf("请猜数：");
          scanf("%d", &guess_num);
          printf("\n");          
      }
      success:
          printf("恭喜！您猜中了！\n按q退出，其它继续！\n");
          continue;
      fail:
          printf("很遗憾！您没猜中！\n按q退出，其它继续！\n");
          continue;
  }
  return 0;
}
